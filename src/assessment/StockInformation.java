package stockInformation;

import webService.WebServiceInterface;

import stockInformation.exceptions.FailedToLoginException;
import stockInformation.exceptions.PasswordTooShortException;
import stockInformation.exceptions.SymbolInvalidException;
import stockInformation.exceptions.UserIDGreaterThan9999Exception;
import stockInformation.exceptions.UserIDLessThan1Exception;

public class StockInformation{

	private WebServiceInterface webService;

	private int userID = 0;
	private String symbol = "";
	private boolean available = false;
	private boolean exists = false;
	private String companyName = "";
	private int currentPrice = 0;
	private int numberOfSharesOutstanding = 0;
	private int marketCapitalisationInMillions = 0;

	public StockInformation(WebServiceInterface webService, int userID, String password, String symbol) throws UserIDLessThan1Exception, UserIDGreaterThan9999Exception, FailedToLoginException, PasswordTooShortException, SymbolInvalidException{
		this.webService = webService;
		if(userID < 1){
			throw new UserIDLessThan1Exception();
		}else if(userID > 9999){
			throw new UserIDGreaterThan9999Exception();
		}else if(password.length() < 8){
			throw new PasswordTooShortException();
		}if(this.webService.authenticate(userID, password)){
			this.setUserID(userID);
			this.setSymbol(symbol);
		}else{
			this.setCompanyName("Not allowed");
			throw new FailedToLoginException();
		}
	}

	@Override
	public String toString(){
		if(this.getAvailable()){
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(this.getCompanyName());
			strBuilder.append(' ');
			strBuilder.append('[');
			strBuilder.append(this.getSymbol());
			strBuilder.append(']');
			strBuilder.append(' ');
			strBuilder.append(this.getCurrentPrice());
			return strBuilder.toString();
		}else{
			return "Not Found";
		}
	}

	public int getUserID(){
		return userID;
	}

	public String getSymbol(){
		return symbol;
	}

	public Boolean getAvailable(){
		return available;
	}

	public Boolean getExists(){
		return exists;
	}

	public String getCompanyName(){
		return companyName;
	}

	public int getCurrentPrice(){
		return currentPrice;
	}

	public int getNumberOfSharesOutstanding(){
		return numberOfSharesOutstanding;
	}

	public int getMarketCapitalisationInMillions(){
		return marketCapitalisationInMillions;
	}

	private void setUserID(int userID){
		if(userID > 0 && userID < 10000){
			this.userID = userID;
		}
	}

	public void setSymbol(String symbol) throws SymbolInvalidException{
		String regexPattern = "^([A-z0-9]*)$";
		if(symbol.matches(regexPattern)){
			this.symbol = symbol.toUpperCase();
			this.setStockInfo(this.webService.getStockInfo(symbol));
		}else{
			throw new SymbolInvalidException();
		}
	}

	private void setAvailable(Boolean available){
		this.available = available;
	}

	private void setExists(Boolean exists){
		this.exists = exists;
	}

	private void setCompanyName(String companyName){
		if(companyName != null && !companyName.equals("")){
			this.companyName = companyName;
			this.setExists(true);
		}else{
			this.setExists(false);
		}
	}

	private void setCurrentPrice(int currentPrice){
		this.currentPrice = currentPrice;
	}

	private void setNumberOfSharesOutstanding(int numberOfSharesOutstanding){
		this.numberOfSharesOutstanding = numberOfSharesOutstanding;
	}

	private void setMarketCapitalisationInMillions(int marketCapitalisationInMillions){
		if(marketCapitalisationInMillions >= 0){
			this.setAvailable(true);
			this.marketCapitalisationInMillions = marketCapitalisationInMillions;
		}else{
			this.setAvailable(false);
		}
	}

	private void setStockInfo(String stockInfo){
		if(stockInfo.contains(",") && stockInfo.split(",").length == 4){
			String[] splitString = stockInfo.split(",");
			this.setCompanyName(splitString[1]);
			this.setCurrentPrice(Integer.parseInt(splitString[2]));
			this.setNumberOfSharesOutstanding(Integer.parseInt(splitString[3]));
			if(this.getNumberOfSharesOutstanding() >= 0){
				this.setMarketCapitalisationInMillions(this.getCurrentPrice() * this.getNumberOfSharesOutstanding());
			}else{
				this.setMarketCapitalisationInMillions(-1);
			}
		}else{
			this.setCompanyName("");
			this.setCurrentPrice(0);
			this.setNumberOfSharesOutstanding(0);
			this.setMarketCapitalisationInMillions(-1);
		}
	}
}
