package stockInformation;

import static org.junit.Assert.*;

import org.easymock.EasyMockSupport;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import webService.WebServiceInterface;

import stockInformation.exceptions.FailedToLoginException;
import stockInformation.exceptions.PasswordTooShortException;
import stockInformation.exceptions.SymbolInvalidException;
import stockInformation.exceptions.UserIDGreaterThan9999Exception;
import stockInformation.exceptions.UserIDLessThan1Exception;

public class StockInformationTest extends EasyMockSupport{

	private WebServiceInterface webServiceMock; // Collaborator

	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception{
	}

	@Before
	public void setUp() throws Exception{
		this.webServiceMock = EasyMock.createMock(WebServiceInterface.class); // Instantiate a New Mock of The WebService Class (Based On The Interface)
	}

	@After
	public void tearDown() throws Exception{
	}

	/**
	 * ID: 1
	 * Test 1.1
	 */
	@Test
	public void getUserID_Valid_Test(){
		int userID = 10;
		String password = "P4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(10, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.authenticate(20, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.authenticate(30, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.authenticate(40, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo(symbol)).andReturn("ABC123,Test Company,2000,2000").anyTimes();
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 10", stockInformation.getUserID() == userID);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			userID = 20;
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 20", stockInformation.getUserID() == userID);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			userID = 30;
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 30", stockInformation.getUserID() == userID);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			userID = 40;
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 40", stockInformation.getUserID() == userID);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 2
	 * Test 1.2
	 */
	@Test
	public void constructor_Boundary_Test_Lower(){
		int userID = 0;
		String password = "P4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(1, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo(symbol)).andReturn("ABC123,Test Company,2000,2000");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			assertTrue("User ID Was Less Than 1", true);
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			userID = -1;
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			assertTrue("User ID Was Less Than 1", true);
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			userID = 1;
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 1", stockInformation.getUserID() == userID);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 3
	 * Test 1.3
	 */
	@Test
	public void constructor_Boundary_Test_Upper(){
		int userID = 10000;
		String password = "P4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(9999, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo(symbol)).andReturn("ABC123,Test Company,2000,20000").anyTimes();
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			assertTrue("User ID Was Greater Than 9999", true);
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			userID = 10001;
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			assertTrue("User ID Was Greater Than 9999", true);
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			userID = 9999;
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 9999", stockInformation.getUserID() == userID);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 4
	 * Test 1.4
	 */
	@Test
	public void getSymbol_Valid_Test(){
		int userID = 10;
		String password = "P4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(userID, password)).andReturn(true).anyTimes();
		EasyMock.expect(this.webServiceMock.getStockInfo("ABC123")).andReturn("ABC123,Test Company,2000,2000");
		EasyMock.expect(this.webServiceMock.getStockInfo("DEF456")).andReturn("DEF456,Company Test,3000,2500");
		EasyMock.expect(this.webServiceMock.getStockInfo("GHI789")).andReturn("GHI789,,0,0");
		EasyMock.expect(this.webServiceMock.getStockInfo("JKL012")).andReturn("JKL012,,0,0");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("Symbol Was 'ABC123'", stockInformation.getSymbol().equals(symbol));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			symbol = "DEF456";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("Symbol Was 'DEF456'", stockInformation.getSymbol().equals(symbol));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			symbol = "GHI789";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("Symbol Was 'GHI789'", stockInformation.getSymbol().equals(symbol));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			symbol = "JKL012";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("Symbol Was 'JKL012'", stockInformation.getSymbol().equals(symbol));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 5
	 * Test 1.5
	 */
	@Test
	public void constructor_InvalidPassword_Test(){
		int userID = 10;
		String password = "NOTP4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(userID, "NOTP4$$W0rd")).andReturn(false);
		EasyMock.expect(this.webServiceMock.authenticate(userID, "ASDFASDF")).andReturn(false);
		EasyMock.expect(this.webServiceMock.authenticate(userID, "P4$$W0rd")).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo(symbol)).andReturn("ABC123,Test Company,2000,2000");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			assertTrue("Failed To Login", true);
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			password = "ASDFASDF";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			assertTrue("Failed To Login", true);
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			password = "P4$$W0rd";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("Login Was Successfull", !stockInformation.getCompanyName().equals("Not allowed"));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 6
	 * Test 1.6
	 */
	@Test
	public void constructor_InvalidUserID_Test(){
		int userID = 8;
		String password = "P4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(8, password)).andReturn(false);
		EasyMock.expect(this.webServiceMock.authenticate(9, password)).andReturn(false);
		EasyMock.expect(this.webServiceMock.authenticate(10, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo(symbol)).andReturn("ABC123,Test Company,2000,2000");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			assertTrue("Failed To Login", true);
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			userID = 9;
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			assertTrue("Failed To Login", true);
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			userID = 10;
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("Login Was Successfull", !stockInformation.getCompanyName().equals("Not allowed"));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 7
	 * Test 2.1
	 */
	@Test
	public void getters_Valid_Test(){
		int userID = 10;
		String password = "P4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(userID, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo(symbol)).andReturn("ABC123,Test Company,2000,2000");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 10", stockInformation.getUserID() == userID);
			assertTrue("Symbol Was 'ABC123'", stockInformation.getSymbol().equals(symbol));
			assertTrue("Market With Symbol 'ABC123' Exists", stockInformation.getExists());
			assertTrue("Market With Symbol 'ABC123' Is Available", stockInformation.getAvailable());
			assertTrue("Company Name Was 'Test Company'", stockInformation.getCompanyName().equals("Test Company"));
			assertTrue("Current Price Was '2000'", stockInformation.getCurrentPrice() == 2_000);
			assertTrue("Number of Outstanding Shares Was '2000'", stockInformation.getNumberOfSharesOutstanding() == 2_000);
			assertTrue("Market Capitalisation In Millions Was '4000000'", stockInformation.getMarketCapitalisationInMillions() == 4_000_000);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 8
	 * Test 2.2
	 */
	@Test
	public void getters_Invalid_Test(){
		int userID = 50;
		String password = "P4$$W0rd";
		String symbol = "DEF123";

		EasyMock.expect(this.webServiceMock.authenticate(50, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.authenticate(10, password)).andReturn(true).anyTimes();
		EasyMock.expect(this.webServiceMock.getStockInfo("DEF123")).andReturn("").anyTimes();
		EasyMock.expect(this.webServiceMock.getStockInfo("DEF456")).andReturn("DEF456,Unavailable Test Company,0,-1");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 10", stockInformation.getUserID() == userID);
			assertTrue("Symbol Was 'DEF123'", stockInformation.getSymbol().equals(symbol));
			assertTrue("Market With Symbol 'DEF123' Does Not Exist", !stockInformation.getExists());
			assertTrue("Market With Symbol 'DEF123' Is Not Available", !stockInformation.getAvailable());
			assertTrue("Company Name Was ''", stockInformation.getCompanyName().equals(""));
			assertTrue("Current Price Was '0'", stockInformation.getCurrentPrice() == 0);
			assertTrue("Number of Outstanding Shares Was '0'", stockInformation.getNumberOfSharesOutstanding() == 0);
			assertTrue("Market Capitalisation In Millions Was '0'", stockInformation.getMarketCapitalisationInMillions() == 0);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		userID = 10;

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 10", stockInformation.getUserID() == userID);
			assertTrue("Symbol Was 'DEF123'", stockInformation.getSymbol().equals(symbol));
			assertTrue("Market With Symbol 'DEF123' Does Not Exist", !stockInformation.getExists());
			assertTrue("Market With Symbol 'DEF123' Is Not Available", !stockInformation.getAvailable());
			assertTrue("Company Name Was ''", stockInformation.getCompanyName().equals(""));
			assertTrue("Current Price Was '0'", stockInformation.getCurrentPrice() == 0);
			assertTrue("Number of Outstanding Shares Was '0'", stockInformation.getNumberOfSharesOutstanding() == 0);
			assertTrue("Market Capitalisation In Millions Was '0'", stockInformation.getMarketCapitalisationInMillions() == 0);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			symbol = "DEF456";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("User ID Was 10", stockInformation.getUserID() == userID);
			assertTrue("Symbol Was 'DEF456'", stockInformation.getSymbol().equals(symbol));
			assertTrue("Market With Symbol 'DEF456' Exists", stockInformation.getExists());
			assertTrue("Market With Symbol 'DEF456' Is Not Available", !stockInformation.getAvailable());
			assertTrue("Company Name Was 'Unavailable Test Company'", stockInformation.getCompanyName().equals("Unavailable Test Company"));
			assertTrue("Current Price Was '0'", stockInformation.getCurrentPrice() == 0);
			assertTrue("Number of Outstanding Shares Was '-1'", stockInformation.getNumberOfSharesOutstanding() == -1);
			assertTrue("Market Capitalisation In Millions Was '0'", stockInformation.getMarketCapitalisationInMillions() == 0);
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 9
	 * Test 3.1
	 */
	@Test
	public void toString_Valid_Test(){
		int userID = 10;
		String password = "P4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(userID, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo(symbol)).andReturn("ABC123,Test Company,2000,2000");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("To String Returned 'Test Company [ABC123] 2000'", stockInformation.toString().equals("Test Company [ABC123] 2000"));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 10
	 * Test 3.2
	 */
	@Test
	public void toString_Invalid_Test(){
		int userID = 10;
		String password = "P4$$W0rd";
		String symbol = "XYZ999";

		EasyMock.expect(this.webServiceMock.authenticate(userID, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo(symbol)).andReturn("");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("To String Returned 'Not Found'", stockInformation.toString().equals("Not Found"));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 11
	 * Test 4.1
	 */
	@Test
	public void toString_ValidSetters_Test(){
		int userID = 10;
		String password = "P4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(userID, password)).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo("ABC123")).andReturn("ABC123,Test Company,2000,2000");
		EasyMock.expect(this.webServiceMock.getStockInfo("DEF456")).andReturn("DEF456,Company Test,3000,2500");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			symbol = "DEF456";
			stockInformation.setSymbol(symbol);
			assertTrue("To String Returned 'Company Test [DEF456] 3000'", stockInformation.toString().equals("Company Test [DEF456] 3000"));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 12
	 * Test 4.2
	 */
	@Test
	public void toString_InvalidSetters_Test(){
		int userID = 10;
		String password = "P4$$W0rd";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(userID, password)).andReturn(true).anyTimes();
		EasyMock.expect(this.webServiceMock.getStockInfo("ABC123")).andReturn("ABC123,Test Company,2000,2000").anyTimes();
		EasyMock.expect(this.webServiceMock.getStockInfo("XYZ999")).andReturn("").anyTimes();
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			symbol = "XYZ999";
			stockInformation.setSymbol(symbol);
			assertTrue("To String Returned 'Not Found'", stockInformation.toString().equals("Not Found"));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			symbol = "XYZ999";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			symbol = "ABC123";
			stockInformation.setSymbol(symbol);
			assertTrue("To String Returned 'Test Company [ABC123] 2000'", stockInformation.toString().equals("Test Company [ABC123] 2000"));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Too Short");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 13
	 * Test E1
	 */
	@Test
	public void password_InvalidLength_Test(){
		int userID = 10;
		String password = "P4$$W";
		String symbol = "ABC123";

		EasyMock.expect(this.webServiceMock.authenticate(userID, "P4$$W0rd")).andReturn(true);
		EasyMock.expect(this.webServiceMock.getStockInfo(symbol)).andReturn("ABC123,Test Company,2000,2000");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			assertTrue("Password Was Shorter Than 8 Characters Long", true);
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			password = "P4$$W0";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			assertTrue("Password Was Shorter Than 8 Characters Long", true);
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			password = "P4$$W0r";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			assertTrue("Password Was Shorter Than 8 Characters Long", true);
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}

		try{
			password = "P4$$W0rd";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("Login Was Successfull", !stockInformation.getCompanyName().equals("Not allowed"));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Was Shorter Than 8 Characters Long");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}

	/**
	 * ID: 14
	 * Test E2
	 */
	@Test
	public void symbol_Invalid_Test(){
		int userID = 10;
		String password = "P4$$W0rd";
		String symbol = "ABC!£$";

		EasyMock.expect(this.webServiceMock.authenticate(userID, password)).andReturn(true).anyTimes();
		EasyMock.expect(this.webServiceMock.getStockInfo("ABC123")).andReturn("ABC123,Test Company,2000,2000");
		EasyMock.replay(this.webServiceMock);

		try{
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Was Shorter Than 8 Characters Long");
		}catch(SymbolInvalidException symbolInvalidException){
			assertTrue("Symbol Invalid", true);
		}

		try{
			symbol = "ABC1£$";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Was Shorter Than 8 Characters Long");
		}catch(SymbolInvalidException symbolInvalidException){
			assertTrue("Symbol Invalid", true);
		}

		try{
			symbol = "ABC12$";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			fail("Logged In");
		}catch(FailedToLoginException failedToLoginException){
			fail("Attempted Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Was Shorter Than 8 Characters Long");
		}catch(SymbolInvalidException symbolInvalidException){
			assertTrue("Symbol Invalid", true);
		}

		try{
			symbol = "ABC123";
			StockInformation stockInformation = new StockInformation(this.webServiceMock, userID, password, symbol);
			assertTrue("Symbol Valid", !stockInformation.getCompanyName().equals("Not allowed"));
		}catch(FailedToLoginException failedToLoginException){
			fail("Failed To Login");
		}catch(UserIDGreaterThan9999Exception userIDGreaterThan9999Exception){
			fail("User ID Was Greater Than 9999");
		}catch(UserIDLessThan1Exception userIDLessThan1Exception){
			fail("User ID Was Less Than 1");
		}catch(PasswordTooShortException passwordTooShortException){
			fail("Password Was Shorter Than 8 Characters Long");
		}catch(SymbolInvalidException symbolInvalidException){
			fail("Symbol Invalid");
		}
	}
}
